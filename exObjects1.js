const pessoa = {
    nome: "Thyago",
    idade: 20,
    altura: 1.80,
    peso: 75,
    bolosFavoritos: ["Morango", "Chocolate"],
    endereço: {
        rua:"Dos bobos",
        numero: 20,

    },
    imc() {
        const calculo = (pessoa.peso / pessoa.altura**2).toFixed(2)
        // return 'Seu imc é ' + calculo + " " + pessoa.nome
        return `Seu imc é ${calculo} ${pessoa.nome}`
    }
}
console.log(pessoa.imc())

console.log(pessoa.bolosFavoritos)


const pessoa2 = pessoa 
pessoa2.nome = "João"
console.log(pessoa2)

// Não é possível componentizar dessa forma, como o instrutor falou. A melhor forma é com uma função.

// Outro exemplo


function criaPaciente(nome, peso, altura){

return {
    nome: nome,
    peso: peso,
    altura: altura,
    imc(){
        const calculo = (peso/ altura**2).toFixed(2)
        return `Seu imc: ${calculo} ${nome}`
    }

}
}
const paciente1 = criaPaciente("Thyago", 76, 1.81)
paciente1.peso = 80
console.log(paciente1.imc())

// const paciente2 = criaPaciente()
// const paciente3 = criaPaciente()


// É mais facil dessa forma pois é simples de criar o objeto para cada paciente.
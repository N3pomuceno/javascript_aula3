function soma( ... valores) {
    console.log(valores.length)
    return valores.reduce( (contador, proximo)  => contador + proximo);
}
console.log(soma(1,2,3,4,5))




// Outro exemplo de Rest

let pessoa = {
    nome: "Thyago",
    idade: 20 ,
    cpf: 32165498769
};
const {... pessoa2} = pessoa
// com isso cria uma separação entre as variáveis que, se vc mudar algo depois, o outro não se altera junto.
pessoa2.idade = 18
console.log(pessoa2, pessoa)



// Outro exemplo para spread
const numeros = [1, 65, 86, 27]
console.log(Math.max( ... numeros))



// Mais exemplos nos arquivos da aula
